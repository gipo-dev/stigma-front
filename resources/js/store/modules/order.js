let order = window.localStorage.getItem('order');

let emptyOrder = {
    delivery_id: null,
    user_id: null,
    is_active: true,
    bill: null,
    name: null,
    surname: null,
    father_name: null,
    phone: null,
    payment_type: 'cash',
    delay: null,
    cashback_paid: null,
    card: null,
    payment_screenshot: null,
    cart: null,
};

export default {
    state: {
        order:  order ? JSON.parse(order) :emptyOrder,
    },

    actions: {
        setOrderCart({ state, rootState, commit }) {
            let orderCart = rootState.cart;
            commit('orderCart', orderCart);
            this.commit('saveOrder');
        },

    },

    mutations: {
        setOrderBill(state, bill) {
            state.order.bill = bill;
            this.commit('saveOrder');
        },
        setOrderUserInfo(state, user) {

            state.order.name = user.name;
            state.order.surname = user.surname;
            state.order.father_name = user.father_name;
            state.order.phone = user.phone;
            state.order.user_id = user.id;
            this.commit('saveOrder');

        },
        setOrderDelivery(state, delivery_id) {
            state.order.delivery_id = delivery_id;
            this.commit('saveOrder');
        },
        setOrderPaymentType(state, payment_type) {
            state.order.payment_type = payment_type;
            this.commit('saveOrder');
        },
        setOrderPaymentCard(state, card) {
            state.order.card = card;
            this.commit('saveOrder');
        },
        setOrderDelay(state, delay) {
            state.order.delay = delay;
            this.commit('saveOrder');
        },
        setOrderCashback(state, cashback) {
            state.order.cashback_paid = cashback;
            this.commit('saveOrder');
        },
        orderCart(state, cart) {
            state.order.cart = cart;
            this.commit('saveOrder');
        },
        saveOrder(state) {
            window.localStorage.setItem('order', JSON.stringify(state.order));
        },
        clearOrder(state) {
            state.order = emptyOrder; 
            window.localStorage.removeItem('order');
        }
    },

    getters: {

    }
}