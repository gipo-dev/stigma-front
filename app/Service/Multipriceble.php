<?php

namespace App\Service;

trait Multipriceble
{
    /**
     * @var \App\Service\PriceManager
     */
    private $_priceManager;

    /**
     * @return \App\Service\PriceManager
     */
    public function multiprice()
    {
        if (!$this->_priceManager)
            $this->_priceManager = new PriceManager($this);
        return $this->_priceManager;
    }


    public function getMinPriceAttribute()
    {
        return $this->multiprice()->getMinPrice();
    }
}
