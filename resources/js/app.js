require('./bootstrap');

window.Vue = require('vue').default;

Vue.component('V-header', require('./components/V-header.vue').default);
Vue.component('V-footer', require('./components/V-footer.vue').default);
Vue.component('MobileMenu', require('./components/MobileMenu.vue').default);

import router from './router';
import store from './store';

const app = new Vue({
    el: '#app',
    router,
    store,
    created() {
        const userInfo = localStorage.getItem('user')
        if (userInfo) {
            const userData = JSON.parse(userInfo)
            this.$store.commit('setUserData', userData)
        }
        axios.interceptors.response.use(
            response => response,
            error => {
                if (error.response.status === 401) {
                    this.$store.dispatch('logout')
                }
                return Promise.reject(error)
            }
        )
    },
});