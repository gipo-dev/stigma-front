<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SavedDelivery extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'type',
        'adress',
        'city',
        'pickup_point',
        'passport_series',
        'passport_number',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
