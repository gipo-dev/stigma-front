<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NewDeliveryController extends Controller
{

    public function httpGet($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    public function getCities()
    {
        $cities_json = $this->httpGet('http://integration.cdek.ru/v1/location/cities/json');
        $cities_json = json_decode($cities_json);

        return $cities_json;
    }
}
