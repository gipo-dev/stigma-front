let cart = window.localStorage.getItem('cart');

import Product from "../../services/Product";

export default {

    state: {
        cart: cart ? JSON.parse(cart) : [],
    },

    actions: {},

    mutations: {
        addToCart(state, product) {
            let productInCartIndex = state.cart.findIndex(item => item.id === product.id);
            if (productInCartIndex !== -1) {
                let oldQuantity = state.cart[productInCartIndex].quantity;
                let newQuantity = product.quantity;
                state.cart[productInCartIndex].quantity = 0;
                state.cart[productInCartIndex].quantity = newQuantity + oldQuantity;
                this.commit('saveCart');
                return
            } else {
                state.cart.push(product);
                this.commit('saveCart');
            }

        },
        saveCart(state) {
            window.localStorage.setItem('cart', JSON.stringify(state.cart));
        },
        removeFromCart(state, index) {
            state.cart.splice(index, 1);
            this.commit('saveCart');
        },
        updateCart(state, cart) {
            state.cart = cart;
            this.commit('saveCart');
        },
        updateQuantity(state, payload) {
            if (payload.val == '-') {
                state.cart[payload.index].quantity--;
                this.commit('saveCart');
                return
            } else if (payload.val == '+') {
                state.cart[payload.index].quantity++;
                this.commit('saveCart');
                return
            }
            state.cart[payload.index].quantity = payload.val;
            this.commit('saveCart');
        },
        clearCart(state) {
            state.cart = [];
            localStorage.removeItem('cart');
        }
    },

    getters: {
        getCartCount(state) {
            return state.cart.length;
        },
        getCart(state) {
            return state.cart;
        },
        getCartTotal(state) {
            let total = 0;

            state.cart.forEach(product => {
                total += Product.getProductPrice(product, product.quantity) * product.quantity;
            });

            return total;
        }
    }
}
