<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderEvents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Jenssegers\Date\Date;

class OrdersController extends Controller
{

    public function index(Request $request)
    {
        return response()->json([
            'orders' => $request->user()->orders()->latest()->with('order_delivery', 'order_events')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $order = new Order();

        if ($request->hasFile('payment_screenshot')) {
            $image = $request->file('payment_screenshot');
            $teaser_image = time() . Str::random(5) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $image->move($destinationPath, $teaser_image);
            $order->payment_screenshot = $teaser_image;
        }

        $request = json_decode($request->order, true);

        $order->user_id = $request['user_id'];
        $order->delivery_id = $request['delivery_id'];
        $order->is_active = true;
        $order->bill = $request['bill'];
        $order->name = $request['name'];
        $order->surname = $request['surname'];
        $order->father_name = $request['father_name'];
        $order->phone = $request['phone'];
        $order->payment_type = $request['payment_type'];
        $order->delay = $request['delay'];
        $order->cashback_paid = $request['cashback_paid'];
        $order->card = $request['card'];
        $order->cart = json_encode($request['cart']);

        $order->save();

        $order_event = new OrderEvents();
        $order_event->order_id = $order->id;
        $order_event->type = 'gray';
        $order_event->is_active = true;
        $order_event->title = 'Принят';
        $order_event->text = 'Заказ принят магазином';
        $order_event->date = Date::now()->format('d F');
        $order_event->time = Carbon::now()->format('H:i');
        $order_event->save();

        if ($order->payment_type == 'card') {
            if ($order->payment_screenshot !== null) {
                $order_event = new OrderEvents();
                $order_event->order_id = $order->id;
                $order_event->type = 'processing_payment';
                $order_event->is_active = true;
                $order_event->title = 'Оплата в обработке';
                $order_event->text = 'Ваша оплата проверятеся магазином, пожалуйста, подождите';
                $order_event->date = Date::now()->format('d F');
                $order_event->time = Carbon::now()->format('H:i');
                $order_event->save();
            } else {
                $order_event = new OrderEvents();
                $order_event->order_id = $order->id;
                $order_event->type = 'waiting_payment';
                $order_event->is_active = true;
                $order_event->title = 'Ожидает оплаты';
                $order_event->text = 'Оплатите заказ и прикрепите скриншоты оплаты, чтобы заказ начал обрабатываться';
                $order_event->date = Date::now()->format('d F');
                $order_event->time = Carbon::now()->format('H:i');
                $order_event->save();
            }
        }

        return response($order);
    }

    public function attach_payment_screenshot(Request $request, $id)
    {

        if ($request->file('payment_screenshot')) {

            $order = Order::where('id', $id)->first();

            $image = $request->file('payment_screenshot');
            $teaser_image = time() . Str::random(5) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $image->move($destinationPath, $teaser_image);
            $order->payment_screenshot = $teaser_image;
            $order->save();

            $order_waiting_event = OrderEvents::where([['order_id', $id], ['type', 'waiting_payment']])->first();
            $order_waiting_event->is_active = false;
            $order_waiting_event->save();

            $order_event = new OrderEvents();

            $order_event->order_id = $id;
            $order_event->type = 'processing_payment';
            $order_event->is_active = true;
            $order_event->title = 'Оплата в обработке';
            $order_event->text = 'Ваша оплата проверятеся магазином, пожалуйста, подождите';
            $order_event->date = Date::now()->format('d F');
            $order_event->time = Carbon::now()->format('H:i');
            $order_event->save();

            return response('success');
        }

        return response('Error! You need to attach file first!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
