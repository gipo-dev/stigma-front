export default {
    state: {
        deliveries: [],
    },

    actions: {
        fetchDeliveries(ctx, _) {
            axios
                .get('/api/deliveries')
                .then((response) => {
                    ctx.commit('setDeliveries', response.data);
                })
        },
        addDelivery(state, delivery) {
            axios
                .get('/api/deliveries/create', {
                    params: delivery,
                })
                .then((response) => {
                    console.log(response)
                    state.dispatch('fetchDeliveries');
                })
        },
    },

    mutations: {
        setDeliveries(state, deliveriesData) {
            state.deliveries = deliveriesData.deliveries;
        }
    },

    getters: {
        allDeliveries(state) {
            return state.deliveries;
        }
    }
}
