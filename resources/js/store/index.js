import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import product from './modules/product';
import cart from './modules/cart';
import order from './modules/order';
import orders from './modules/orders';
import delivery from './modules/delivery';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: null
    },

    mutations: {
        setUserData(state, userData) {
            state.user = userData
            localStorage.setItem('user', JSON.stringify(userData))
            axios.defaults.headers.common.Authorization = `Bearer ${userData.token}`
        },
        clearUserData() {
            localStorage.removeItem('user')
            location.reload()
        },
        setUpdatedData(state, userData) {
            state.user.user = userData;
            localStorage.setItem('user', JSON.stringify(state.user));
        }
    },

    actions: {
        login({ commit }, credentials) {
            return axios
                .post('/api/do_login', credentials)
                .then(({ data }) => {
                    commit('setUserData', data)
                })
        },
        updateUser({commit}, userData) {
            console.log(userData);
            commit('setUpdatedData', userData);
        },
        logout({ commit }) {
            commit('clearUserData')
        }
    },

    getters: {
        isLogged: state => !!state.user,
        getUserOrderInfo(state) {
            let userOrderInfo = {
                name: '',
                surname: '',
                father_name: '',
                phone: '',
                id: ''
            };

            userOrderInfo.name = state.user.user.name;
            userOrderInfo.surname = state.user.user.surname;
            userOrderInfo.father_name = state.user.user.father_name;
            userOrderInfo.phone = state.user.user.phone;
            userOrderInfo.id = state.user.user.id;

            return userOrderInfo;
        },
        getUserCashback(state) {
            return  state.user.user.cashback;
        }
    },

    modules: {
        product,
        cart,
        order,
        orders,
        delivery,
    },
})
