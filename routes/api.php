<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\UserController;
use App\Http\Controllers\Api\V1\ProductController;
use App\Http\Controllers\Api\V1\DeliveryController;
use App\Http\Controllers\Api\V1\OrdersController;
use App\Http\Controllers\Api\V1\ChatsController;
use App\Http\Controllers\Api\V1\NewDeliveryController;

Route::middleware('auth:sanctum')->resource('/user', UserController::class);
Route::middleware('auth:sanctum')->post('/user/update', [UserController::class, 'updateUser']);
Route::get('/user/clients', [UserController::class, 'get_clients']);
/*
Route::resource('/user', UserController::class);
*/

Route::post('/do_login', [AuthController::class, 'do_login']);

Route::group(['middleware' => ['auth:sanctum']], function () {

    Route::resource('/products', ProductController::class);

    Route::resource('/orders', OrdersController::class);
    Route::post('/orders/{id}/attach_payment', [OrdersController::class, 'attach_payment_screenshot']);

    Route::resource('deliveries', DeliveryController::class);

    Route::get('/new-delivery/get-cities', [NewDeliveryController::class, 'getCities']);

    Route::get('messages/{id}', [ChatsController::class, 'fetchMessages']);
    Route::post('messages', [ChatsController::class, 'sendMessage']);

});
