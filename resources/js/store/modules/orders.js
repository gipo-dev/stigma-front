export default {
    state: {
        orders: []
    },

    actions: {
        fetchOrders({ commit }) {
            const orders = axios
                .get('/api/orders')
                .then(response => {
                    commit('setOrders', response.data.orders)
                });
        }
    },

    mutations: {
        setOrders(state, ordersData) {
            state.orders = ordersData;
        }
    },

    getters: {
        activeOrders(state) {
            state.orders.forEach(order => {
                let date = order.created_at.split('T')[0];
                date = date.split('-');
                date = date[2] + '.' + date[1] + '.' + date[0];
                order.date = date;
                let orderSumm = 0;
                let orderCart = JSON.parse(order.cart);
                orderCart.cart.forEach(item => {
                    orderSumm += item.quantity * item.price;
                });
                order.summ = orderSumm;
            });
            return state.orders.filter(order => order.is_active == true);
        },
        doneOrders(state) {
            state.orders.forEach(order => {
                let date = order.created_at.split('T')[0];
                date = date.split('-');
                date = date[2] + '.' + date[1] + '.' + date[0];
                order.date = date;
                let orderSumm = 0;
                let orderCart = JSON.parse(order.cart);
                orderCart.cart.forEach(item => {
                    if(item.quantity <= 60 ) {
                    orderSumm += 400 * item.quantity;
                }
                if(item.quantity > 60 && item.quantity <= 120 ) {
                    orderSumm += 395 * item.quantity;
                }
                if(item.quantity > 120 ) {
                    orderSumm += 390 * item.quantity;
                }
                });
                order.summ = orderSumm;
            });
            return state.orders.filter(order => order.is_active == false);
        },
        allOrders(state) {
            state.orders.forEach(order => {
                let date = order.created_at.split('T')[0];
                date = date.split('-');
                date = date[2] + '.' + date[1] + '.' + date[0];
                order.date = date;
                let orderSumm = 0;
                let orderCart = JSON.parse(order.cart);
                orderCart.cart.forEach(item => {
                    if(item.quantity <= 60 ) {
                    orderSumm += 400 * item.quantity;
                }
                if(item.quantity > 60 && item.quantity <= 120 ) {
                    orderSumm += 395 * item.quantity;
                }
                if(item.quantity > 120 ) {
                    orderSumm += 390 * item.quantity;
                }
                });
                order.summ = orderSumm;
            });
            return state.orders;
        },
    }
}
