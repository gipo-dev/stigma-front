<?php

namespace App\Service;

use App\Models\ProductType;

class PriceManager
{
    /**
     * @var \App\Models\ProductType
     */
    private $product;

    /**
     * @param \App\Models\ProductType $product
     */
    public function __construct(ProductType $product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getMinPrice()
    {
        return $this->product->prices->min('price');
    }
}
