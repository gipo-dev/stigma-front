<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public function order_delivery() {
        return $this->belongsTo('App\Models\SavedDelivery', 'delivery_id');
    } 
    public function order_events()
    {
        return $this->hasMany('App\Models\OrderEvents');
    }

}
