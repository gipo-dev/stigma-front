<?php

namespace App\Models;

use App\Service\Multipriceble;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    use HasFactory;
    use Multipriceble;

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'price',
        'product_id',
        'description',
        'quantity',
        'photo',
        'parent_name',
    ];

    /**
     * @var string[]
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var string[]
     */
    protected $with = [
        'prices',
    ];

    /**
     * @var string[]
     */
    protected $appends = ['min_price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany(ProductPrice::class, 'product_id');
    }

}
