import vueRouter from 'vue-router';
import Vue from 'vue';

Vue.use(vueRouter);

import Login from './views/Login';
//REMOVE ON PROD
import Logout from './views/Logout';
//REMOVE ON PROD
import Register from './views/Register';
import Catalog from './views/Catalog';
import Account from './views/Account';
import Product from './views/Product';
import Error404 from './views/Error404';
import Cart from './views/Cart';
import Checkout from './views/Checkout';
import CheckoutBill from './components/checkout/Bill.vue';
import CheckoutUser from './components/checkout/User.vue';
import CheckoutDelivery from './components/checkout/Delivery.vue';
import CheckoutPayment from './components/checkout/Payment.vue';
import Orders from './views/Orders';
import Privacy from './views/Privacy';
import About from './views/About';
import Success from './views/Success';
import Order from './views/Order';
import OrderDetails from './components/order/Details';
import OrderPayment from './components/order/Payment';
import OrderChat from './components/order/Chat';

const routes = [
    {
        path: '/login',
        component: Login
    },
    {
        path: '/logout',
        component: Logout
    },
    {
        path: '/register',
        component: Register
    },
    {
        path: '/',
        redirect: '/products',
        meta: {
            auth: true
        },
    },
    {
        path: '/products',
        component: Catalog,
        name: 'Catalog',
        meta: {
            auth: true
        },
    },
    {
        path: '/products/:id',
        component: Product,
        name: 'Product',
        props: true,
        meta: {
            auth: true
        },
    },
    {
        path: '/cart',
        component: Cart,
        name: 'Cart',
        meta: {
            auth: true
        },
    },
    {
        path: '/checkout',
        component: Checkout,
        name: 'Checkout',
        meta: {
            auth: true
        },
        children: [
            {
                path: 'bill',
                component: CheckoutBill,
                name: 'CheckoutBill',
            },
            {
                path: 'user',
                component: CheckoutUser,
                name: 'CheckoutUser',
            },
            {
                path: 'delivery',
                component: CheckoutDelivery,
                name: 'CheckoutDelivery',
            },
            {
                path: 'payment',
                component: CheckoutPayment,
                name: 'CheckoutPayment',
            },
        ]
    },
    {
        path: '/orders',
        component: Orders,
        meta: {
            auth: true
        },
    },
    {
        path: '/orders/:id',
        component: Order,
        name: 'Order',
        props: true,
        meta: {
            auth: true
        },
    },
    {
        path: '/account',
        component: Account,
        meta: {
            auth: true
        },
    },
    {
        path: '/about',
        component: About,
        name: 'About',
        meta: {
            auth: true
        },
    },
    {
        path: '/privacy-policy',
        component: Privacy,
        name: 'Privacy',
        meta: {
            auth: true
        },
    },
    {
        path: '/success',
        component: Success,
        name: 'Success',
        meta: {
            auth: true
        },
    },
    {
        path: '/404',
        component: Error404,
        name: 'Error404',
        meta: {
            auth: true
        },
    },
    {
        path: '*',
        redirect: '/404',
        meta: {
            auth: true
        },
    }

];

const router = new vueRouter({
    mode: 'history',
    routes,
});

router.beforeEach((to, from, next) => {
    const isLoggedIn = localStorage.getItem('user');

    if (to.matched.some(record => record.meta.auth) && !isLoggedIn) {
        next('/login')
        return
    }

    next()
});

export default router;