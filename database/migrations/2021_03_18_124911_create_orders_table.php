<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('delivery_id');
            $table->boolean('is_active');
            $table->string('bill')->nullable();
            $table->string('name');
            $table->string('surname');
            $table->string('father_name');
            $table->string('phone');
            $table->string('payment_type');
            $table->string('delay')->nullable();
            $table->string('cashback_paid')->nullable();
            $table->string('card')->nullable();
            $table->string('payment_screenshot')->nullable();
            $table->json('cart')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
