export default {

    getProductPrice(product, count) {
        let currentPrice = 0;
        product.prices.forEach((price) => {
            if (price.to == null || currentPrice < price.to)
                currentPrice = price.price;
        });
        return currentPrice;
    }

}
