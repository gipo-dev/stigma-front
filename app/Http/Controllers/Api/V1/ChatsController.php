<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Message;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Jenssegers\Date\Date;

class ChatsController extends Controller
{
    public function fetchMessages(Request $request, $id)
    {
        $user = $request->user();
        return Message::where('chat_id', $id)->orderBy('created_at', 'asc')->with('user')->get();
    }

    public function sendMessage(Request $request)
    {
        if ($request->hasFile('attachment')) {

            $user = $request->user();

            $image = $request->file('attachment');
            $teaser_image = time() . Str::random(5) . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $image->move($destinationPath, $teaser_image);

            $messageFromJson = $request->input('message');
            $messageFromJson = json_decode($messageFromJson);

            $message = new Message();

            if($messageFromJson->{'message'} == '') {
                $message->message = 'unset';
            } else {
                $message->message = $messageFromJson->{'message'};
            }
            $message->chat_id = $messageFromJson->{'id'};
            $message->user_id = $user->id;
            $message->type = 'attachment';
            $message->attachment = $teaser_image;
            $message->time = Carbon::now()->format('H:i');
            $message->date = Date::now()->format('d F');

            $message->save();

            return response()->json($message);
            
        } else {
            $user = $request->user();

            $messageFromJson = $request->json()->all();

            $message = new Message();

            $message->message = $messageFromJson['message'];
            $message->chat_id = $messageFromJson['id'];
            $message->user_id = $user->id;
            $message->type = 'message';
            $message->time = Carbon::now()->format('H:i');
            $message->date = Date::now()->format('d F');

            $message->save();

            return response()->json($message);
        }
    }
}
