export default {
    state: {
        products: []
    },

    actions: {
        fetchProducts({ commit }) {
            const products = axios
                .get('/api/products')
                .then(response => {
                    commit('setProducts', response.data)
                });
        }
    },

    mutations: {
        setProducts(state, productsData) {
            state.products = productsData;
        }
    },

    getters: {
        allProducts(state) {
            return state.products;
        }
    }
}
